package com.spacemetric.test.eclipse.transitive.issue.a;

import java.util.ServiceLoader;

public class A {

	public static void main(String[] args) {
		ServiceLoader<Plugin> plugins = ServiceLoader.load(Plugin.class);
		for (Plugin plugin : plugins) {
			plugin.printSomething();
		}
	}
}
