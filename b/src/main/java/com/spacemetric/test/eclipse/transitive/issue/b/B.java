package com.spacemetric.test.eclipse.transitive.issue.b;

import com.spacemetric.test.eclipse.transitive.issue.a.Plugin;
import com.spacemetric.test.eclipse.transitive.issue.c.C;

public class B implements Plugin {

	@Override
	public void printSomething() {
		C.printHello();
	}

}
