# Eclipse runconfig transitive dependency issue

Eclipse seems to have a problem in that when a dependency is added to the run configuration, none of its dependencies are added which often leads to a NoClassDefFoundError during runtime.
This project demonstrates this by having 3 projects: a, b and c with 3 classes A, B and C and one interface Plugin.

Project b has a dependency on project a and c while projects a and c have no dependencies at all.
Class A in project a uses the ServiceLoader to load instances of the Plugin interface and iterates over them.
Class B in project b implements this interface and uses class C from project c in its implementation.

In order to run the application we create a run configuration for class A and then add our plugin (project b) to the dependencies.
My assumption would be that any dependencies of b would also be added to the classpath by eclipse but this is not the case. Instead the result when running is a NoClassDefFoundError.
This error could of course easily be resolved by adding project c as well to the dependencies list. But in a real world scenario using ServiceLoader to load plugins into an application, it is often very difficult to find ALL the correct dependencies that needs to be added and doing this work manually.
I see very little reason why eclipse should not resolve these dependencies by itself.
 